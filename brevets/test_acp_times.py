import acp_times

test_start_time = "2017-01-01T00:00:00-08:00"

def test_base():
    print("Base case")
    assert(acp_times.open_time(0, 200, test_start_time) == test_start_time)

def test_turnover1():
    print("Turnover at 200km")
    print("Expected: 2017-01-01T05:52:56.470588-08:00")
    print("Result: " + acp_times.open_time(200.0, 200.0, test_start_time))
    assert(acp_times.open_time(200.0, 200.0, test_start_time) == "2017-01-01T05:52:56.470588-08:00")
    assert(acp_times.open_time(200.0, 400.0, test_start_time) == "2017-01-01T06:15:00-08:00")

def test_turnover2():
    print("Turnover at 400km")
    assert(acp_times.open_time(400.0, 400.0, test_start_time) == "2017-01-01T12:30:00-08:00")
    assert(acp_times.open_time(400.0, 600.0, test_start_time) == "2017-01-01T13:20:00-08:00")

def test_turnover3():
    print("Turnover at 600km")
    assert(acp_times.open_time(600.0, 600.0, test_start_time) == "2017-01-01T20:00:00-08:00")
    assert(acp_times.open_time(600.0, 1000.0, test_start_time) == "2017-01-01T21:25:42.857143-08:00")

def test_edge():
    print("Above 1000km")
    assert(acp_times.open_time(1001.0, 1000.0, test_start_time) == "2017-01-02T11:45:00-08:00")

